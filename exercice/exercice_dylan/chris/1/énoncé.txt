Exercice 1 :

Une boite de dialogue doit s'afficher et inviter l'utilisateur � entrer une phrase.
Il faut ensuite afficher sur la page HTML
    - La phrase
    - Le nombre de caract�res (un espace est un caract�re);
    
Exemple :
    J'entre "Bonjour !"
    J'affiche donc "Bonjour !" et 9.
    
Tant que l'utilisateur n'entre pas au moins une lettre tu lui affiches la boite de dialogue.
S'il entre un nombre : r�affiche la boite de dialogue
S'il n'entre rien : r�affiche la boite de dialogue
S'il entre une lettre ou plus : c'est ok tu appelles la fonction ecrireReponsePhrase avec les param�tres.

Exemple :
    J'entre "20"
    Je r�affiche la boite de dialogue car ce n'est pas une phrase

    Jentre "7 voitures"
    C'est ok ! j'appelle ecrireReponsePhrase

Pour afficher le contenu dans la page HTML il y a une fonction d�j� pr�te : ecrireReponsePhrase
Pour l'utiliser, tu dois lui envoyer deux param�tres : La phrase et la taille.
Tu n'as pas besoin de modifier la fonction, elle fonctionne d�j�. 
Je ne sais pas si tu connais les fonctions, du coup je l'ai fait pour toi. 


Pour appeler une fonction :
nomDeLaFonction(param�tre1, param�tre2, param�tre3, etc...);

Les fonctions : 
https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Fonctions

Taille d'une chaine de caract�res : 
https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String/length

 